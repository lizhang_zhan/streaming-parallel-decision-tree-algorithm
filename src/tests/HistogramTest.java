package tests;

import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.junit.Test;

import de.uniBonn.lab.dataMining.spdt.histogram.Histogram;
import de.uniBonn.lab.dataMining.spdt.histogram.Histogram.Bin;

public class HistogramTest {
	
	private final static int MAX_BIN_NUMBER = 10;
	private double [] numbers = {23, 19, 10, 16, 36, 2, 9 , 32, 30, 45};
	//private double [] numbers = {25, 21, 12, 18, 38, 4, 11 , 34, 32, 47};
	private static double epsilon = 0.02;
	
	@Test
	public void testUpdate() 
	{
		Histogram hist = new Histogram(MAX_BIN_NUMBER);
		for(int i = 0; i <numbers.length; i++) {
	    	hist.addPoint(numbers[i]);
	    }
		NavigableMap<Double, Integer> expected = new TreeMap<Double, Integer>();
	    expected.put(2.0, 1);
	    expected.put(9.5, 2);
	    expected.put(19.33, 3);
	    expected.put(32.67, 3);
	    expected.put(45.0, 1);
	    //hist.printBins();
	    assertTrue(IsHistEequal(hist.getBinsSet(), expected));
	    expected.put(45.0, 2);
	    assertTrue(!IsHistEequal(hist.getBinsSet(), expected));
	}
	
	
	@Test
	public void testMerge() 
	{
		Histogram hist = new Histogram(MAX_BIN_NUMBER);
		for(int i = 0; i <MAX_BIN_NUMBER; i++) {
	    	hist.addPoint(numbers[i]);
	    }
		Histogram toMerge = new Histogram(MAX_BIN_NUMBER);
		for(int i = MAX_BIN_NUMBER; i <numbers.length; i++) {
			toMerge.addPoint(numbers[i]);
	    }
		hist.mergeHistogram(toMerge);
		NavigableMap<Double, Integer> expected = new TreeMap<Double, Integer>();
	    expected.put(2.0, 1);
	    expected.put(9.5, 2);
	    expected.put(19.33, 3);
	    expected.put(32.67, 3);
	    expected.put(45.0, 1);
	    //hist.printBins();
	    assertTrue(IsHistEequal(hist.getBinsSet(), expected));
	    expected.put(45.0, 2);
	    assertTrue(!IsHistEequal(hist.getBinsSet(), expected));
	}
	
	
	
	@Test
	public void testSum() 
	{
		Histogram hist = new Histogram(MAX_BIN_NUMBER);
		for(int i = 0; i <numbers.length; i++) {
	    	hist.addPoint(numbers[i]);
	    }
		double smallerThanVal = 15;
		double numSmallerThan = hist.sum(smallerThanVal);
		
		double expected = 3.28;
		System.out.println("num of points smaller than " + smallerThanVal + " :" + numSmallerThan);
		assertTrue((Math.abs(numSmallerThan - expected)) <= epsilon);
	}
	
	@Test
	public void testUniform() 
	{
		Histogram hist = new Histogram(MAX_BIN_NUMBER);
		for(int i = 0; i <numbers.length; i++) {
	    	hist.addPoint(numbers[i]);
	    }
		double [] result = hist.uniform(3);
		for(int i = 0 ; i < result.length; i++) {
			System.out.println("uniform result: " + result[i]);
		}
		//double [] result = {15.2101, 28.98};
		SortedSet <Double> expected = new TreeSet<Double>();
		expected.add(15.21);
		expected.add(28.98);
		
		double sum1 = hist.sum(15.21);
		double sum2 = hist.sum(15.21,28.98);
		double sum3 = hist.sum(28.98, 50);
		System.out.println("sum:" + sum1);
		System.out.println("sum:" + sum2);
		System.out.println("sum:" + sum3);
	    assertTrue(arraysEqual(result, expected));
	    double expectedNumPoints = 3.333;
	    assertTrue((Math.abs(sum1 - expectedNumPoints)) <= epsilon);
	    //assertTrue((Math.abs(sum2 - expectedNumPoints)) <= epsilon);
	    //assertTrue((Math.abs(sum3 - expectedNumPoints)) <= epsilon);
	}
	
	
	private boolean arraysEqual(double[] result, SortedSet<Double> expected) {
		for(double curRes : result) {
			if(expected.subSet(curRes - epsilon, curRes + epsilon).size() < 1)
				return false;
		}
		return true;
	}


	private boolean IsHistEequal(NavigableMap<Double, Bin> result, NavigableMap<Double, Integer> expected) {
		for(double curExp : expected.keySet()) {
			Set<Entry<Double, Bin>> matchingBin = result.subMap(curExp - epsilon,
					curExp + epsilon).entrySet();
			
			if (matchingBin.size() == 1) {
				for (Map.Entry<Double, Bin> curBin : matchingBin) {
					if(curBin.getValue().getCount() != expected.get(curExp))
					return false;
				}
			}
		}
		return true;
	}

}
