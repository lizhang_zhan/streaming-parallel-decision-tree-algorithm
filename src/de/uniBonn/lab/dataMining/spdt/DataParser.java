package de.uniBonn.lab.dataMining.spdt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import de.uniBonn.lab.dataMining.spdt.compositeFields.DataPair;
import de.uniBonn.lab.dataMining.spdt.compositeFields.DataSample;

/**
 * the class responisble for parsing the input data into a format readable by
 * the application
 * 
 * @author Rania
 *
 */
public class DataParser {

	public static DataPair<List<DataSample>, Collection<String>> parseData(String filename) {

		File file = new File(filename);
		BufferedReader br;
		Collection <String> classes = new HashSet<String>();
		try {
			br = new BufferedReader(new FileReader(file));

			String row = br.readLine();
			String[] attList = row.split(",");
			//String classId = attList[attList.length - 1];
			List<DataSample> dataSamples = new ArrayList<DataSample>();
			while ((row = br.readLine()) != null) {

				String[] values = row.split(",");
				int i = 0;
				DataSample dataSample = new DataSample();
				String classLabel = values[values.length - 1];
				dataSample.setClassLabel(classLabel);
				classes.add(classLabel);
				for (String curValue : values) {
					if (i == values.length - 1)
						continue;
					double value = Double.valueOf(curValue);
					String attName = attList[i];
					dataSample.addAttValue(attName, value);
					++i;
				}
				dataSamples.add(dataSample);
				i = 0;
			}
			br.close();
			DataPair<List<DataSample>, Collection<String>> data = new DataPair<List<DataSample>, Collection<String>>(dataSamples, classes);
			return data;
		} catch (Exception e) {
			System.err.println("Exception occured while parsing the input data");
			e.printStackTrace();
		}
		return null;
	}
}
