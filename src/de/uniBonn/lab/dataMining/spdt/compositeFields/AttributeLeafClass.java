package de.uniBonn.lab.dataMining.spdt.compositeFields;

/**
 * a class that represents an attribute, leaf and a class (used as a key in the histograms
 * maps that are created based on a leaf, attribute and class: h(v,i, i)) 
 * @author Rania
 *
 */
public class AttributeLeafClass {
	
	private AttributeLeaf attLeaf;
	private String classLabel;
	
	public AttributeLeaf getAttLeaf() {
		return attLeaf;
	}

	public void setAttLeaf(AttributeLeaf attLeaf) {
		this.attLeaf = attLeaf;
	}


	
	public String getClassLabel() {
		return classLabel;
	}

	public void setClassLabel(String classLabel) {
		this.classLabel = classLabel;
	}

	@Override
	public boolean equals(Object other) {
		if(!(other instanceof AttributeLeafClass)) {
			return false;
		}
		AttributeLeafClass otherAttValClass = (AttributeLeafClass)other;
		if((classLabel == null && otherAttValClass.getClassLabel()!=null) || (otherAttValClass.getClassLabel()==null && classLabel!=null) ) {
			return false;
		}
		//boolean classLabelEqual = false;
		/*if(classLabel == null && otherAttValClass.getClassLabel() == null)
			classLabelEqual = true;
		else*/ 
		boolean classLabelEqual = (otherAttValClass.getClassLabel().equals(classLabel));
		boolean equals = classLabelEqual && otherAttValClass.getAttLeaf().equals(attLeaf);
		//System.out.println(equals);
	    return equals;
	}
	
	@Override
	public int hashCode() {
		int hash = 1;
		hash = hash * 31 + attLeaf.hashCode(); 
	    hash = hash * 31 + classLabel.hashCode();
	    return hash;
	}	
}