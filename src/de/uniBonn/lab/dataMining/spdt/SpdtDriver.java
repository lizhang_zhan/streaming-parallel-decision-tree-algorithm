package de.uniBonn.lab.dataMining.spdt;

import java.util.Collection;
import java.util.List;

import de.uniBonn.lab.dataMining.spdt.compositeFields.DataPair;
import de.uniBonn.lab.dataMining.spdt.compositeFields.DataSample;


public class SpdtDriver {

	public static void main(String[] args) throws InterruptedException {
		DataPair<List<DataSample>, Collection<String>> data1 = DataParser.parseData("breast-cancer-wisconsin1.csv");
		List<DataSample> dataSamples1 = data1.getData1();
		List<DataSample> dataSamples2 = DataParser.parseData("breast-cancer-wisconsin2.csv").getData1();
		
		
		MasterProcessor master = new MasterProcessor();
		Collection <String> classes = data1.getData2();
		Collection <String> attributes = dataSamples1.get(0).getAttValueMap().keySet();

		master.setClassesAtts(classes, attributes);
		int candidatesNum = 5;
		WorkerProcessor worker1 = new WorkerProcessor(candidatesNum, dataSamples1, master.getTree());
		WorkerProcessor worker2 = new WorkerProcessor(candidatesNum, dataSamples2, master.getTree());
		master.addWorker(worker1);
		master.addWorker(worker2);
		WorkerThread workerT1 = new WorkerThread(worker1);
		WorkerThread workerT2 = new WorkerThread(worker2);
		
		runBatch(master, worker1, worker2, workerT1, workerT2);
		runBatch(master, worker1, worker2, workerT1, workerT2);
		runBatch(master, worker1, worker2, workerT1, workerT2);	
		runBatch(master, worker1, worker2, workerT1, workerT2);
		runBatch(master, worker1, worker2, workerT1, workerT2);
		runBatch(master, worker1, worker2, workerT1, workerT2);
		runBatch(master, worker1, worker2, workerT1, workerT2);
		runBatch(master, worker1, worker2, workerT1, workerT2);
		runBatch(master, worker1, worker2, workerT1, workerT2);
		//dataSamples1 = DataParser.parseData("breast-cancer-wisconsin1.csv").getData1();
		//dataSamples2 = DataParser.parseData("breast-cancer-wisconsin2.csv").getData1();
		//worker1.setDataSamples(dataSamples1);
		//worker2.setDataSamples(dataSamples2);
		/*runBatch(master, worker1, worker2, workerT1, workerT2);
		runBatch(master, worker1, worker2, workerT1, workerT2);
		runBatch(master, worker1, worker2, workerT1, workerT2);
		runBatch(master, worker1, worker2, workerT1, workerT2);
		runBatch(master, worker1, worker2, workerT1, workerT2);
		runBatch(master, worker1, worker2, workerT1, workerT2);
		*/
		//master.getTree().printTree();
		for(DataSample dataInstance: dataSamples1) {
			System.out.print("classified value: " + master.getTree().classifySample(dataInstance));
			System.out.println(", true value: " + dataInstance.getClassLabel());
		}
	}
	
	private static void runBatch(MasterProcessor master, WorkerProcessor worker1, WorkerProcessor worker2, WorkerThread workerT1, WorkerThread workerT2) {
		Thread workerThread1 = new Thread(workerT1);
		Thread workerThread2 = new Thread(workerT2);
		workerThread1 .start();
		workerThread2.start();
		try {
			workerThread1.join();
			workerThread2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		master.receiveHistogram(worker1.getLocalHistMap());
		master.receiveHistogram(worker2.getLocalHistMap());
		master.onFinishMerge();	
	}
}
