package de.uniBonn.lab.dataMining.spdt.histogram;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Arrays;
import java.util.NavigableMap;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * 
 * @author Rania
 *
 */
public class Histogram {

	private static final double epsilon = 0.001;

	/**
	 * the maximum number of Bins allowed in the histogram
	 */
	private int maxBins;

	/**
	 * the set of bins held by this histogram
	 */
	private NavigableMap<Double, Bin> binsSet = new TreeMap<Double, Bin>();

	private double minPoint = Double.MAX_VALUE; // p0
	private double maxPoint = Double.MIN_VALUE;// pB+1

	private double totalNumPoints = -1;
	
	NavigableMap<Double, Double> pointsSums;

	/**
	 * the total number of points compressed in this histogram
	 */
	//private int totalNumPoints = 0;
	
	public Histogram(Histogram hist) {
		binsSet = new TreeMap<Double, Bin>(hist.getBinsSet());
		this.minPoint = hist.getMinPoint();
		this.maxPoint = hist.getMaxPoint();
		//this.totalNumPoints = hist.getTotalNumPoints();
		this.maxBins = hist.getMaxBins();
		//pointsSums = new TreeMap<Double, Double>(hist.getPointSums());
	}

	private int getMaxBins() {
		return maxBins;
	}

	private double getMaxPoint() {
		return maxPoint;
	}

	private double getMinPoint() {
		return minPoint;
	}

	public Histogram(int maxBins) {
		this.maxBins = maxBins;
	}

	/**
	 * this the update procedure from the paper (algorithm 1): adds a point to
	 * the histogram
	 * 
	 * @param point
	 */
	public void addPoint(double point) {
		Set<Entry<Double, Bin>> matchingBin = binsSet.subMap(point - epsilon,
				point + epsilon).entrySet();
		if (matchingBin.size() > 0) {
			for (Map.Entry<Double, Bin> curBin : matchingBin) {
				curBin.getValue().incrementCount();
				break;
			}
		} else {
			addNewBin(point);
		}
		updateMinMax(point);
		normalizeBins();
		//++totalNumPoints;
	}

	public double getTotalNumPoints() {
		if(totalNumPoints < 0) {
			totalNumPoints = 0 ;
			for (double curPoint : binsSet.keySet()) {
				totalNumPoints+= binsSet.get(curPoint).getCount();
			}
			//System.out.println("total: " + totalNumPoints);
		}
		return totalNumPoints;
	}

	public void addNewBin(double point) {
		Bin newBin = new Bin(point);
		//newBin.incrementCount();
		binsSet.put(point, newBin);
	}

	public void addNewBin(double point, int binCount) {
		Bin newBin = new Bin(point);
		newBin.setCount(binCount);
		binsSet.put(point, newBin);
	}

	/**
	 * merge new histogram with this histogram so that the number of bins in the
	 * new histogram does not exceed the maximum number allowed
	 * 
	 * @param newHistogram
	 */
	public void mergeHistogram(Histogram newHistogram) {
		for (double curPoint : newHistogram.getBinsSet().keySet()) {
			Set<Entry<Double, Bin>> matchingBinSet = binsSet.subMap(
					curPoint - epsilon, curPoint + epsilon).entrySet();
			if (matchingBinSet.size() > 0) {
				for (Map.Entry<Double, Bin> matchingBin : matchingBinSet) {
					matchingBin.getValue().addCount(newHistogram.getBinsSet().get(curPoint).getCount());
					break;
				}
			} else {
				//Bin newBin = new Bin(newHistogram.getBinsSet().get(curPoint));
				//binsSet.put(curPoint, newBin);
				binsSet.put(curPoint, newHistogram.getBinsSet().get(curPoint));
			}
		}
		updateMinMax(newHistogram.getMaxPoint());
		updateMinMax(newHistogram.getMinPoint());
		normalizeBins();
	}

	public NavigableMap<Double, Bin> getBinsSet() {
		return binsSet;
	}

	/**
	 * return the estimated number of points in the interval
	 * [interval.getRightEnd(), interval.getLeftEnd()]
	 * 
	 * @param point
	 * @return
	 */

	/**
	 * the the number of point in the interval [leftPoint, rightPoint]
	 * 
	 * @param leftPoint
	 * @param rightPoint
	 * @return
	 */
	public double sum(double leftPoint, double rightPoint) {
		return sum(rightPoint) - sum(leftPoint);
	}

	/**
	 * return the estimated number of points in the interval [-infinity, point]
	 * 
	 * @param point
	 * @return
	 */
	public double sum(double givenPoint) {
		double edgeSum = handleEdgeSum(givenPoint);
		if (edgeSum >= -0.001) {
			//System.out.println("edge");
			return edgeSum;
		}
	
		Entry<Double, Bin> low = binsSet.floorEntry(givenPoint);
		Entry<Double, Bin> high = binsSet.higherEntry(givenPoint);
		double lowCount = low.getValue().getCount();
		double highCount = high.getValue().getCount();

		double lowPoint = low.getKey();
		double highPoint = high.getKey();
		double mb = lowCount
				+ (((double) highCount - lowCount) / (highPoint - lowPoint))
				* (givenPoint - lowPoint);
		double s = ((lowCount + mb) / 2)
				* ((givenPoint - lowPoint) / (highPoint - lowPoint));
		double lowestPoint = binsSet.firstKey();
		SortedMap<Double, Bin> leftToPoint = binsSet.subMap(lowestPoint,
				lowPoint);
		for (Double curPoint : leftToPoint.keySet()) {
			s += leftToPoint.get(curPoint).getCount();
		}
		s += (double) (lowCount) / 2;
		return s;
	}

	// handle the edge cases for the subroutine sum
	private double handleEdgeSum(double givenPoint) {
		if (givenPoint < minPoint) {
			//System.out.println("smaller than min");
			return 0;
		}
		if (givenPoint < binsSet.firstKey()) {
			double mb = (binsSet.get(binsSet.firstKey()).getCount())
					/ (binsSet.firstKey() - minPoint) * (givenPoint - minPoint);
			double s = (mb / 2) * (givenPoint - minPoint)
					/ (binsSet.firstKey() - minPoint);
			//System.out.println("min: " + s);
			return s;
		}
		if (givenPoint > maxPoint || givenPoint >= binsSet.lastKey()) {
			/*double mi = (binsSet.get(binsSet.lastKey()).getCount());
			double mb = mi - mi*(givenPoint - binsSet.lastKey())
					/ (maxPoint - binsSet.lastKey());
			double s = ((binsSet.get(binsSet.lastKey()).getCount() + mb) / 2)
					* (givenPoint - binsSet.lastKey())
					/ (maxPoint - binsSet.lastKey());
			*/
			//System.out.println("max num points");
			return getTotalNumPoints();
		}
		
		return -1;
	}

	/**
	 * calculate the candidate split points
	 * 
	 * @param optimalNumBins
	 *            the number of candidate split points
	 * @return
	 */
	public double[] uniform(int candNum) {
		double[] splitValues = new double[candNum - 1];
		// for each point find sum(-infinity, this point), key is the sum, value
		// is the point
		if(pointsSums == null || pointsSums.isEmpty())
			calculatePointSums();
		double totalPoints = getTotalNumPoints();
		for (int j = 1; j <= candNum - 1; j++) {
			double s = (((double) j) / candNum) * totalPoints;
			// double[] lowHighPoints = findInterMediateSumPoint(s, pointsSums);
			// System.out.println(s);
			Double lowSum = pointsSums.lowerKey(s);
			// TODO
			if (lowSum == null) {
				//System.out.println("null 1");
				lowSum = pointsSums.floorKey(s);
			}
			
			double lowPoint;
			if(lowSum == null) {
				lowPoint = binsSet.firstKey();
				lowSum = 0.0;
			}
			else
				lowPoint = pointsSums.get(lowSum);
			Double higherKey = pointsSums.higherKey(s);
			double highPoint;
			if (higherKey != null)
				highPoint = pointsSums.get(higherKey);
			else {
				highPoint = pointsSums.get(binsSet.lastKey());
			}
			double d = s - lowSum;
			int lowCount = binsSet.get(lowPoint).getCount();
			int highCount = binsSet.get(highPoint).getCount();
			double muj = HistogramUtil.findUj(highPoint, lowPoint, highCount,
					lowCount, d, s);
			splitValues[j - 1] = muj;
		}
		//System.out.println("split values found");
		//System.out.println(Arrays.toString(splitValues));
		return splitValues;
	}

	/*
	 * private double[] findInterMediateSumPoint(double s, SortedSet<Double>
	 * pointsSums) { double interMediatePoints[] = new double[2];
	 * 
	 * interMediatePoints[0] = pointsSums.headSet(s).last();
	 * interMediatePoints[1] = pointsSums.tailSet(s+epsilon).first(); return
	 * interMediatePoints; }
	 */

	/**
	 * calculate sum for each point in the histogram
	 */
	private void calculatePointSums() {
		pointsSums = new TreeMap<Double, Double>();
		double sum = 0;
		for (Double curPoint : binsSet.keySet()) {
			sum = sum(curPoint);
			pointsSums.put(sum, curPoint);
		}
	}

	/**
	 * normalize the number of bins so that it is equal to the maximum allowed
	 */
	private void normalizeBins() {
		while (binsSet.size() > maxBins) {
			double minDiff = Double.MAX_VALUE;
			// System.out.println("min diff:" + minDiff );
			double minimmizerPoint = 0;
			double minimmizerNextPoint = 0;
			Iterator<Double> pointsIter = binsSet.keySet().iterator();
			Double prevPoint = pointsIter.next();
			while (pointsIter.hasNext()) {
				Double nextPoint = pointsIter.next();
				if (nextPoint - prevPoint < minDiff) {
					minDiff = nextPoint - prevPoint;
					minimmizerPoint = prevPoint;
					minimmizerNextPoint = nextPoint;
				}
				prevPoint = nextPoint;
			}
			int minimizerCount = binsSet.get(minimmizerPoint).getCount();
			int minimizerNextCount = binsSet.get(minimmizerNextPoint)
					.getCount();
			binsSet.remove(minimmizerPoint);
			binsSet.remove(minimmizerNextPoint);
			double newPoint = (minimmizerPoint * minimizerCount + minimmizerNextPoint
					* minimizerNextCount)
					/ (minimizerCount + minimizerNextCount);
			addNewBin(newPoint, minimizerCount + minimizerNextCount);
		}
	}
	/**
	 * 
	 * @author Rania
	 *
	 */
	public static class Bin {

		/**
		 * the point represented by this Bin
		 */
		private double point;

		/**
		 * the number of values in the bin range
		 */
		private int count = 0;
		
		public Bin(Bin other) {
			this.point = other.point;
			this.count = other.count;
		}
		
		public Bin(double point) {
			this.point = point;
			count = 1;
		}

		public void setCount(int binCount) {
			if(binCount > 700) {
				System.out.println();
			}
			this.count = binCount;

		}

		public double getPoint() {
			return point;
		}

		public void setPoint(double point) {
			this.point = point;
		}

		public void incrementCount() {
			if(count > 700) {
			System.out.println("inc");
			}
			++count;
		}

		public int getCount() {
			return count;
		}

		public void addCount(int count2) {
			count += count2;
		}
	}

	public void printBins() {
		for (double curPoint : binsSet.keySet()) {
			System.out.println(curPoint + ","
					+ binsSet.get(curPoint).getCount());
		}
	}

	/**
	 * the min and max points comprise the interval where all the points are
	 * located
	 * 
	 * @param point
	 */
	private void updateMinMax(double point) {
		if (point < minPoint) {
			minPoint = point;
		} else if (point > maxPoint) {
			maxPoint = point;
		}

	}

	/**
	 * the number of points greater than the given point (used for calculating
	 * the entropy)
	 * 
	 * @param point
	 * @return
	 */
	public double greaterSum(double point) {
		return sum(point, maxPoint);
	}

	/**
	 * the number of points smaller than the given point (used for calculating
	 * the entropy)
	 * 
	 * @param point
	 * @return
	 */
	public double smallerSum(double point) {
		return sum(point);
	}
}
