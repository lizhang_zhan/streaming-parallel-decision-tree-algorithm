package de.uniBonn.lab.dataMining.spdt.histogram;

/**
 * Contains helper (analytical) functions related to the histogram 
 * @author Rania
 *
 */
public class HistogramUtil {

	private static double epsilon = 0.001;

	public static double findUj(double highPoint, double lowPoint, int highCount,
			int lowCount, double d, double s) {
		double uj = 0;
		double approxD;
		int a = (highCount - lowCount);
		int b = 2 * lowCount;
		double c = -2 * d;
		do {
			double z;
			if(a < epsilon) {
				z = -c/b;
			}
			else {
				z = getQuadraticRoot(a, b, c)[0];
			}
			uj = lowPoint + (highPoint - lowPoint) * z;
			double mUj = lowCount
					+ (((double)(highCount) - lowCount) / (highPoint - lowPoint))
					* (uj - lowPoint);
			approxD = (lowCount + mUj) / 2
					* ((uj - lowPoint) / (highPoint - lowPoint));
		} while (!(d >= approxD - epsilon)  && d <=(approxD + epsilon));

		return uj;
	}
	
	/**
	 * get the quadratic roots of the formula given by ax^2 + bx + c
	 * @param a
	 * @param b
	 * @param c
	 * @return
	 */
	public static double[] getQuadraticRoot(int a, int b, double c) {
		double[] roots = new double[2];
		double sqrtRoot = Math.sqrt(Math.pow(b, 2) - 4 * a * c);
		roots[0] = (-b + sqrtRoot)/(2 * a);
		roots[1] = (-b - sqrtRoot)/(2 * a);
		return roots;
	}

}
