package de.uniBonn.lab.dataMining.spdt;

public class WorkerThread implements Runnable{

	private WorkerProcessor worker;
	
	public WorkerThread(WorkerProcessor worker) {
		this.worker = worker;
	}
	@Override
	public void run() {
		worker.compressData();
	}

}
