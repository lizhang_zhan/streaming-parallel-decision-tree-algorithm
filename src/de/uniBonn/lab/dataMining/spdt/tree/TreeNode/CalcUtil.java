package de.uniBonn.lab.dataMining.spdt.tree.TreeNode;

import java.util.Collection;
import java.util.List;

/**
 * a calculation utility class used during the tree construction for calculating
 * entropy and so on.
 * @author Rania
 *
 */
public class CalcUtil {
	
	public enum EntroptType {Gini, Entropy};
	
	private static EntroptType entroptType = EntroptType.Entropy;
	
	public static double calculateImpurity(double... labelProbabilities) {
		if(entroptType == EntroptType.Gini) {
			return calculateGiniImpurity(labelProbabilities);
		}
		else {
			return calculateEntropyImpurity(labelProbabilities);
		}
	}
	public static double calculateGiniImpurity(double... labelProbabilities) {
		double sum = 0;
		for (int i = 0; i < labelProbabilities.length; i++) {
			sum += Math.pow(labelProbabilities[i], 2);
		}
		return 1 - sum;
	}

	public static double calculateEntropyImpurity(double ... labelProbabilities) {
		double sum = 0;
		for (int i = 0; i < labelProbabilities.length; i++) {
			if (labelProbabilities[i] == 0)
				continue;
			sum -= labelProbabilities[i] * log2(labelProbabilities[i]);
		}
		return sum;
	}

	public double calculateGap(double qj, double qlj, double qrj, double leftChildProb) {
		double gap = calculateImpurity(qj) - leftChildProb * calculateImpurity(qlj) - (1-leftChildProb) * calculateImpurity(qrj);
		return gap;
		
	}
	
	/**
	 * return the total entropy and the gain (in order to find out if we have a perfect split, which happens when gain = totalEntropy)
	 * @param labelCount
	 * @param rightCount
	 * @param totalCount
	 * @param labelCountLeft
	 * @param labelCountRight
	 * @return
	 */
	public static double [] calculateEntropyGain(List<Double> labelCount, double rightCount, double totalCount, List<Double> labelCountLeft, List<Double> labelCountRight) {
		double totalEntropy = 0;
		double leftCount = totalCount - rightCount;
		totalEntropy = calculateTotalEntropy(labelCount, totalCount);
		double partialEntropies = calculatePartialEntropy(labelCountLeft, leftCount, totalCount) + 
					calculatePartialEntropy(labelCountRight, rightCount, totalCount);
			
		double gain = totalEntropy - partialEntropies;
		double [] entropyGain = {totalEntropy, gain};
		return entropyGain;
	}
	
	
	public static double calculateTotalEntropy(Collection<Double> labelCount, double totalCount) {

		double totalEntory = 0;
		for (double curCount : labelCount) {
			if (curCount != 0) {
				totalEntory += ( curCount / totalCount)
						* log2((double) totalCount / (double) curCount);
			}
		}
		return totalEntory;
	}
	
	
	public static double calculatePartialEntropy(
			Collection<Double> labelCountDir, double totalDirCount,
			double totalCount) {
		if(totalDirCount < 0.000001)
			return 0;
		double dirTotalEntory = 0;
		double coeff = totalDirCount / totalCount;
		for (double largerCountPerClass : labelCountDir) {
			if (largerCountPerClass > 0.000001)
				dirTotalEntory += (largerCountPerClass / totalDirCount)
						* log2(totalDirCount / largerCountPerClass);
		}
		dirTotalEntory *= coeff;
		return dirTotalEntory;
	}
	

	private static double log2(double x) {
		return (Math.log(x) / Math.log(2));
	}
}
