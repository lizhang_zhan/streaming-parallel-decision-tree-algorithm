package de.uniBonn.lab.dataMining.spdt.tree.TreeNode;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.HashSet;

import de.uniBonn.lab.dataMining.spdt.compositeFields.AttributeLeaf;
import de.uniBonn.lab.dataMining.spdt.compositeFields.AttributeLeafClass;
import de.uniBonn.lab.dataMining.spdt.compositeFields.DataSample;
import de.uniBonn.lab.dataMining.spdt.histogram.Histogram;

/**
 * The decision tree class: contains the functionality related to constructing
 * the decision tree based on the split candidate values that were calculated by
 * the master node
 * 
 * @author Rania
 *
 */
public class DecisionTree {

	private static final double EPSILON = 0.001;
	private int candidatesNum = 20;// this is B~
	private TreeNode rootNode = new TreeNode(true);
	/**
	 * indicates if the tree is being created the first time or simply being
	 * updated
	 */
	private boolean isNewTree = true;
	private List<TreeNode> unlabeledLeaves = new LinkedList<TreeNode>();
	private int height = 0;

	/**
	 * the classes associated with each attribute
	 */
	// private Map<String, List<String>> attClassesMap;
	/**
	 * the set of possible class values
	 */
	private List<String> classes;

	/**
	 * the set of possible attributes
	 */
	private Collection<String> attributes;

	

	public DecisionTree(int candidatesNum) {
		this.candidatesNum = candidatesNum;
	}

	/**
	 * here the tree is being updated from subsequent batch updates received
	 * from the workers
	 * 
	 * @param attLeafClassHistMap
	 * @param attLeafHistMap
	 */
	public void updateTree(
			Map<AttributeLeafClass, Histogram> attLeafClassHistMap,
			Map<AttributeLeaf, Histogram> attLeafHistMap) {
		if (isNewTree) {
			unlabeledLeaves.add(rootNode);
			isNewTree = false;
		}
		List<TreeNode> currentUnlabeledLeaves = new LinkedList<TreeNode>(
				unlabeledLeaves);
		while (!currentUnlabeledLeaves.isEmpty()) {
			EntropyData bestAttSplitGain = new EntropyData("", -1, -1, -1, null, null);
			TreeNode unlabeledLeaf = currentUnlabeledLeaves.get(0);
			unlabeledLeaves.remove(0);
			EntropyData entropyData = null;
			for (String curAtt : attributes) {
				AttributeLeaf attLeaf = new AttributeLeaf();
				attLeaf.setAttribute(curAtt);
				attLeaf.setLeaf(unlabeledLeaf);

				MajorityClassData majority = getCountPerLabel(attLeaf, attLeafClassHistMap);
				if (majority.isPerfectSplit()) {
					unlabeledLeaf.setAttribute("Split is perfect");
					bestAttSplitGain.setMajorityClass(majority.getMajorityClass());
					unlabeledLeaf.setLabeleded(true);
					//System.out.println("perfect");
					break;
				} else if(majority.getMaxCount() > 0 ){ // which means data is arriving to this node
					entropyData = findBestSplit(attLeaf,
							attLeafClassHistMap, attLeafHistMap,
							majority.getLabelsCount());
					determineBestSplit(bestAttSplitGain, entropyData, majority.getMajorityClass());
				} else{
					continue;
				}
			}
			if(bestAttSplitGain.getGain() > 0)
				System.out.println("max gain: " + bestAttSplitGain.getGain() +" best att " + bestAttSplitGain.getAttribute());
			initNode(unlabeledLeaf, bestAttSplitGain);
			createLeftRightChild(unlabeledLeaf, bestAttSplitGain, entropyData);
			currentUnlabeledLeaves.remove(unlabeledLeaf);
			//attributes.remove(bestAttSplitGain.getAttribute());
		}
		++height;
	}

	public Collection<String> getAttributes() {
		return attributes;
	}

	public boolean isNewTree() {
		return isNewTree;
	}

	public void setNewTree(boolean isNewTree) {
		this.isNewTree = isNewTree;
	}

	private void createLeftRightChild(TreeNode unlabeledLeaf,
			EntropyData bestAttSplitGain, EntropyData entropyData) {
		TreeNode leftNode = new TreeNode();
		TreeNode rightNode = new TreeNode();
		leftNode.setLevel(height + 1);
		rightNode.setLevel(height + 1);
		// we would like to have a majority class label from the current butch for the next
		// level, in case that no more data arrives at these leaves after they're created
		if (entropyData != null) {
			String leftMajority = findMajorityClass(entropyData
					.getLeftCountPerLabel().getLabelsCount());
			String rightMajority = findMajorityClass(entropyData
					.getRightCountPerLabel().getLabelsCount());
			leftNode.setMajorityClass(leftMajority);
			rightNode.setMajorityClass(rightMajority);
		}
		
		unlabeledLeaf.setRightNode(rightNode);
		unlabeledLeaf.setLeftNode(leftNode);
		unlabeledLeaves.add(leftNode);
		unlabeledLeaves.add(rightNode);
	}

	private String findMajorityClass(List<Double> labelsCount) {
		int maxClassIndex = 0;
		int totalCount = 0;
		for(int i = 1; i < labelsCount.size(); i++) {
			totalCount+=labelsCount.get(i);
			if(labelsCount.get(i) > labelsCount.get(maxClassIndex)); 
				maxClassIndex = i;
		}
		if(totalCount > 0)
			return classes.get(maxClassIndex);
		return "";
	}

	/**
	 * determine if this current split over all the candidate values per attribute
	 *  is better than the previous best split
	 * @param bestAttSplitGain
	 * @param entData
	 * @param string 
	 */
	private boolean determineBestSplit(EntropyData bestAttSplitGain,
			EntropyData entData, String majorityClass) {
		if (entData.getGain() > bestAttSplitGain.getGain()) {
			bestAttSplitGain.setAttribute(entData.getAttribute());
			bestAttSplitGain.setGain(entData.getGain());
			bestAttSplitGain.setSplitValue(entData.getSplitValue());

			bestAttSplitGain.setLeftCountPerLabel(entData
					.getLeftCountPerLabel());
			bestAttSplitGain.setRightCountPerLabel(entData
					.getRightCountPerLabel());
			bestAttSplitGain.setMajorityClass(majorityClass);
			return true;
		}
		return false;
	}

	/**
	 * find the best split value for the current attribute at this leaf node by
	 * iterating over all the split candidates
	 * 
	 * @param attLeaf
	 * @param attLeafClassHistMap
	 * @param attLeafHistMap
	 * @param labelCount
	 * @return
	 */
	private EntropyData findBestSplit(AttributeLeaf attLeaf,
			Map<AttributeLeafClass, Histogram> attLeafClassHistMap,
			Map<AttributeLeaf, Histogram> attLeafHistMap,
			List<Double> labelCount) {
		Histogram curHist = attLeafHistMap.get(attLeaf);
		double maxGain = -1;
		double bestSplit = 0;
		String bestAtt = null;
		double entropy = 0;
		double[] candidateSplit = curHist.uniform(candidatesNum);

		// List<Double> labelCount = getCountPerLabel(attLeaf,
		// attLeafClassHistMap);
		MajorityClassData leftCountPerLabel = null;
		MajorityClassData rightCountPerLabel = null;
		double total = labelCount.get(2);
		labelCount.remove(2);
		for (double canPoint : candidateSplit) {
			// double smallerTotal = curHist.smallerSum(canPoint);
			double rightTotal = curHist.greaterSum(canPoint);
			rightCountPerLabel = getDirCountPerLabel(attLeaf,
					attLeafClassHistMap, canPoint, false);
			
			leftCountPerLabel = getDirCountPerLabel(attLeaf,
					attLeafClassHistMap, canPoint, true);
			double[] entropyGain = CalcUtil.calculateEntropyGain(labelCount,
					rightTotal, total, leftCountPerLabel.getLabelsCount(),
					rightCountPerLabel.getLabelsCount());
			if (entropyGain[1] > maxGain) {
				maxGain = entropyGain[1];
				bestSplit = canPoint;
				bestAtt = attLeaf.getAtttribute();
				entropy = entropyGain[0];
			}
		}
		EntropyData entData = new EntropyData(bestAtt, entropy, maxGain,
				bestSplit, leftCountPerLabel, rightCountPerLabel);
		return entData;
	}

	private void initNode(TreeNode node, EntropyData bestAttSplitGain) {
		node.setAttribute(bestAttSplitGain.getAttribute());
		node.setSplitValue(bestAttSplitGain.getSplitValue());
		node.setMajorityClass(bestAttSplitGain.getMajorityClass());
	}

	public MajorityClassData getCountPerLabel(AttributeLeaf attLeaf,
			Map<AttributeLeafClass, Histogram> attLeafClassHistMap) {
		double totalCount = 0;
		List<Double> labelCountList = new ArrayList<Double>();
		boolean isPerfect = false;
		double maxCount = 0;
		String majorityClass = "";
		AttributeLeafClass attLeafClass = new AttributeLeafClass();
		for (String curClass : classes) {
			attLeafClass.setClassLabel(curClass);
			attLeafClass.setAttLeaf(attLeaf);
			Histogram attLeafClassCount = attLeafClassHistMap.get(attLeafClass);
			double curCount = 0;
			if(attLeafClassCount != null) {
				curCount= attLeafClassCount.getTotalNumPoints();
			}
		
			if (curCount > maxCount) {
				maxCount = curCount;
				majorityClass = curClass;
			}
			labelCountList.add(curCount);
			totalCount += curCount;
		}
		labelCountList.add(totalCount);
		if (maxCount !=0 && maxCount == totalCount) { // then this is also a perfect split
			isPerfect = true;
		}
		MajorityClassData majorityClassData = new MajorityClassData(
				labelCountList, isPerfect, majorityClass);
		majorityClassData.setMaxCount(maxCount);
		return majorityClassData;

	}

	public TreeNode getTreeRoot() {
		return rootNode;
	}

	/**
	 * get the smaller or larger (direction) count by label (dir means
	 * direction)
	 * 
	 * @param attLeaf
	 * @param attLeafClassHistMap
	 * @return
	 */
	public MajorityClassData getDirCountPerLabel(AttributeLeaf attLeaf,
			Map<AttributeLeafClass, Histogram> attLeafClassHistMap,
			double canPoint, boolean isSmaller) {
		double totalDirCount = 0;
		List<Double> labelCountDir = new ArrayList<Double>();
		boolean isPerfect = false;
		AttributeLeafClass attLeafClass = new AttributeLeafClass();
		double maxCount = 0;
		String majorityClass = "";
		attLeafClass.setAttLeaf(attLeaf);
		for (String curClass : classes) {
			attLeafClass.setClassLabel(curClass);
			double curDirCount;
			if (isSmaller) {
				curDirCount = attLeafClassHistMap.get(attLeafClass).smallerSum(
						canPoint);
				if (curDirCount > maxCount) {
					maxCount = curDirCount;
					majorityClass = curClass;
				}
			} else {
				curDirCount = attLeafClassHistMap.get(attLeafClass).greaterSum(
						canPoint);
				if (curDirCount > maxCount) {
					maxCount = curDirCount;
					majorityClass = curClass;
				}
			}
			labelCountDir.add(curDirCount);
			totalDirCount += curDirCount;
		}
		if (maxCount == totalDirCount) { // then this is also a perfect split
			isPerfect = true;
		}
		MajorityClassData majorityClassData = new MajorityClassData(
				labelCountDir, isPerfect, majorityClass);
		majorityClassData.setMaxCount(maxCount);
		return majorityClassData;
	}

	public List<TreeNode> getUnlabeledLeaves() {
		return unlabeledLeaves;
	}

	public void setClasses(Collection<String> classes) {
		this.classes = new ArrayList<String>(classes);
	}

	public void setAttributes(Collection<String> attributes) {
		this.attributes = attributes;
	}
	
	public void printTree() {
		System.out.println("----Printing the tree----");
		List<TreeNode> q = new ArrayList<TreeNode>();
		q.add(rootNode);
		while (!q.isEmpty()) {
			TreeNode curNode = q.get(0);
			q.remove(0);
			printNode(curNode);
			if (curNode.getLeftNode() != null) {
				if(curNode.getLeftNode().getLeftNode()!=null || curNode.getLeftNode().getRightNode()!=null )
					q.add(curNode.getLeftNode());
			}
			if (curNode.getRightNode() != null) {
				if(curNode.getRightNode().getLeftNode()!=null || curNode.getRightNode().getRightNode()!=null )
				q.add(curNode.getRightNode());
			}
		}
	}

	private void printNode(TreeNode curNode) {
		System.out.println("----");
		System.out.println("Level: " + curNode.getLevel());
		System.out.println("Attribute: " + curNode.getAttribute());
		System.out.println("Best Split: " + curNode.getSplitValue());
		System.out.println("Majority class: " + curNode.getMajorityClass());
	}
	
	
	
	public TreeNode navigateSample(DataSample curDataSample) {
		//TreeNode rootNode = tree.getTreeRoot();
		return navigate(curDataSample, rootNode);
	}

	private TreeNode navigate(DataSample curDataSample, TreeNode node) {
		if (node == null)
			return null;
		if (node.isLabeleded())
			return null;
		if (node.getLeftNode() == null && node.getRightNode() == null) {
			return node;
		}
		String att = node.getAttribute();
		double splitVal = node.getSplitValue();
		double sampleValue = curDataSample.getAttValueMap().get(att);
		if (sampleValue >= splitVal)
			return navigate(curDataSample, node.getRightNode());
		else
			return navigate(curDataSample, node.getLeftNode());
	}
	
	public String classifySample(DataSample dataSample) {
		TreeNode matchingNode = navigateInstance(dataSample);
		if(matchingNode == null)
			return "Unable to classify";
		if(matchingNode.isLabeleded())
			return matchingNode.getClassLabel();
		return matchingNode.getMajorityClass();
	}

	private TreeNode navigateInstance(DataSample instance) {
		return navigateInstanceRec(instance, rootNode);
		
	}

	private TreeNode navigateInstanceRec(DataSample instance, TreeNode node) {
		if (node.isLabeleded())
			return node;
		if (node.getLeftNode() == null && node.getRightNode() == null) {
			return node;
		}
		String att = node.getAttribute();
		double splitVal = node.getSplitValue();
		double sampleValue = instance.getAttValueMap().get(att);
		if (sampleValue >= splitVal)
			return navigate(instance, node.getRightNode());
		else
			return navigate(instance, node.getLeftNode());
	}


}