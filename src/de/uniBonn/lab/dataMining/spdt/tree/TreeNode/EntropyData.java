package de.uniBonn.lab.dataMining.spdt.tree.TreeNode;

class EntropyData {

	private String attribute;
	private double entropy;
	private double gain;
	private double splitValue;
	private String majorityClass;
	private MajorityClassData leftCountPerLabel;
	private MajorityClassData rightCountPerLabel;

	public EntropyData() {

	}

	public EntropyData(String attribute, double entropy, double gain,
			double splitValue, MajorityClassData leftCountPerLabel,
			MajorityClassData rightCountPerLabel) {
		this.attribute = attribute;
		this.entropy = entropy;
		this.gain = gain;
		this.splitValue = splitValue;
		this.leftCountPerLabel = leftCountPerLabel;
		this.rightCountPerLabel = rightCountPerLabel;
	}
	
	public String getMajorityClass() {
		return majorityClass;
	}
	

	public void setMajorityClass(String majorityClass) {
		this.majorityClass = majorityClass;
	}

	public MajorityClassData getLeftCountPerLabel() {
		return leftCountPerLabel;
	}

	public void setLeftCountPerLabel(MajorityClassData leftCountPerLabel) {
		this.leftCountPerLabel = leftCountPerLabel;
	}

	public MajorityClassData getRightCountPerLabel() {
		return rightCountPerLabel;
	}

	public void setRightCountPerLabel(MajorityClassData rightCountPerLabel) {
		this.rightCountPerLabel = rightCountPerLabel;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public double getEntropy() {
		return entropy;
	}

	public void setEntropy(double entropy) {
		this.entropy = entropy;
	}

	public double getGain() {
		return gain;
	}

	public void setGain(double gain) {
		this.gain = gain;
	}

	public double getSplitValue() {
		return splitValue;
	}

	public void setSplitValue(double splitValue) {
		this.splitValue = splitValue;
	}

}
