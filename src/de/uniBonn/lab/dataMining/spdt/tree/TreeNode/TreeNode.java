package de.uniBonn.lab.dataMining.spdt.tree.TreeNode;


public class TreeNode {

	/*public enum NodeType {
		inner, leaf
	};*/

	/**
	 * the label of this node (in case it's a leaf)
	 */
	private String classLabel;
	/**
	 * the node type
	 */
	//private NodeType nodeType;

	/**
	 * this split value in case it's an inner node
	 */

	private Double splitValue = -1.0;
	/**
	 * node to which examples with a smaller value than the split value are
	 * routed
	 */
	private TreeNode leftNode;
	/**
	 * node to which examples with a larger value than the split value are
	 * routed
	 */
	private TreeNode rightNode;

	/**
	 * the name of the attribute represented by this node
	 */
	private String attribute = "";

	/**
	 * the level where this tree node is located
	 */
	private int level = 0;
	/**
	 * indicates wherethis this node is a root node
	 */
	private boolean isRoot;
	
	/**
	 * the majority class even when there's no perfect split on this leaf.
	 */
	private String majorityClass;
	
	/**
	 * indicates if this is a labeled leaf
	 */
	private boolean isLabeleded = false;
	/**
	 * the node id
	 */
	private int id = 0;
	/**
	 * the maximum id of a node created so far across all instances (incremental id)
	 */
	private static int maxid = 0;
	

	public TreeNode(boolean isRoot) {
		this.isRoot = isRoot;
		this.id = maxid;
		maxid++;
	}
	public TreeNode() {
		this(false);
	}

	public TreeNode(String attribute) {
		this.attribute = attribute;
	}

	public void setLeftNode(TreeNode leftNode) {
		this.leftNode = leftNode;
	}
	

	public TreeNode getLeftNode() {
		return leftNode;
	}

	public TreeNode getRightNode() {
		return rightNode;
	}

	public void setRightNode(TreeNode rightNode) {
		this.rightNode = rightNode;
	}

	public double getSplitValue() {
		return splitValue;
	}

	public void setSplitValue(double splitValue) {
		this.splitValue = splitValue;
	}

	public String getClassLabel() {
		return classLabel;
	}

	public void setClassLabel(String classLabel) {
		this.classLabel = classLabel;
	}

	public String getAttribute() {
		return attribute;
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof TreeNode))
			return false;
		TreeNode otherTreeNode = (TreeNode) other;
		/*if(otherTreeNode.isRootNode() && isRoot)
			return true;
		return (otherTreeNode.getLevel() == level
				&& otherTreeNode.isRightChild() == isRightChild && otherTreeNode
				.getAttribute().equals(attribute));*/
		return (otherTreeNode.getId() == id);// && otherTreeNode.getLevel() == level);
	}


	public int getId() {
		return id;
	}
	@Override
	public int hashCode() {
		int hash = 1;
		/*if(classLabel != null)
			hash = hash * 31 + classLabel.hashCode();
		hash = hash * 31 + level;
		hash = hash * 31 + splitValue.hashCode();*/
		hash = hash * 31 + id;
		hash = hash * 31 + level;
		return hash;
	}
	
	public boolean isRootNode(){
		return isRoot;
	}

	public String getMajorityClass() {
		return majorityClass;
	}
	public void setMajorityClass(String majorityClass) {
		this.majorityClass = majorityClass;
	}
	
	
	public boolean isLabeleded() {
		return isLabeleded;
	}
	public void setLabeleded(boolean isLabeleded) {
		this.isLabeleded = isLabeleded;
	}
}
