package de.uniBonn.lab.dataMining.spdt;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.uniBonn.lab.dataMining.spdt.compositeFields.AttributeLeaf;
import de.uniBonn.lab.dataMining.spdt.compositeFields.AttributeLeafClass;
import de.uniBonn.lab.dataMining.spdt.compositeFields.DataSample;
import de.uniBonn.lab.dataMining.spdt.histogram.Histogram;
import de.uniBonn.lab.dataMining.spdt.tree.TreeNode.DecisionTree;
import de.uniBonn.lab.dataMining.spdt.tree.TreeNode.TreeNode;

/**
 * A worker node working collaboratively along with other worker nodes on
 * portions of the whole dataset
 * 
 * @author Rania
 *
 */
public class WorkerProcessor {

	private static int maxBins = 100;

	/**
	 * the current batch being processed by the worker
	 */
	private List<DataSample> dataSamples;

	// an instance of the tree built so far (as received from the master)
	private DecisionTree tree;

	/**
	 * the attributes which are not yet determined as best node att in the
	 * decision tree
	 */
	//private Collection<String> attributes;

	/**
	 * a map of histogram for each attribute-value-class tuple the histogram for
	 * this attribute
	 */
	private Map<AttributeLeafClass, Histogram> localHistMap;

	public WorkerProcessor(DecisionTree tree) {
		this.tree = tree;
		localHistMap = new HashMap<AttributeLeafClass, Histogram>();
	}

	public Map<AttributeLeafClass, Histogram> getLocalHistMap() {
		return localHistMap;
	}

	public WorkerProcessor(int optimalBinsNum, List<DataSample> dataSamples,
			DecisionTree tree) {
		this(tree);
		this.dataSamples = dataSamples;
	}

	public void compressData() {
		if (dataSamples != null && !dataSamples.isEmpty()) {
			compressData(dataSamples);
		}
	}

	/**
	 * the main compression algorithm: algorithm 6
	 * 
	 * @param attValPairs
	 *            : a list of attributes-values-class tuples
	 */
	public void compressData(List<DataSample> dataSamples) {
		//if (tree.isNewTree()) {
		Collection <String>attributes = dataSamples.get(0).getAttValueMap().keySet();
		//} else {
			//attributes = tree.getAttributes();
		//}
		localHistMap.clear();
		for (DataSample curDataSample : dataSamples) {
			TreeNode navigatedLeaf = tree.navigateSample(curDataSample);
			// means we already have a perfect split on this node (labeled leaf)
			if (navigatedLeaf == null) {
				// System.out.println("nav null");
				continue;
			}
			// if the sample is directed to unlabeled leaf v, then update its
			// corresponding histogram
			for (String att : attributes) {
				AttributeLeafClass attLeafClass = new AttributeLeafClass();
				attLeafClass.setAttLeaf(new AttributeLeaf(navigatedLeaf, att));
				attLeafClass.setClassLabel(curDataSample.getClassLabel());
				if (localHistMap.containsKey(attLeafClass)) {
					Histogram curHistogram = localHistMap.get(attLeafClass);
					curHistogram.addPoint(curDataSample.getAttValueMap().get(
							att));
				} else {
					Histogram newHist = new Histogram(maxBins);
					newHist.addPoint(curDataSample.getAttValueMap().get(att));
					localHistMap.put(attLeafClass, newHist);
				}
			}
		}
	}

		/**
	 * update the decision tree received from the master
	 */
	public void updateDecisionTree(DecisionTree updatedTree) {
		this.tree = updatedTree;
	}

	public DecisionTree getTree() {
		return tree;
	}

	public void setTree(DecisionTree tree) {
		this.tree = tree;
	}

	public void setDataSamples(List<DataSample> dataSamples) {
		this.dataSamples = dataSamples;
	}

}
